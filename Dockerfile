# Base images
FROM nginx:latest

# Copy a updated files to the container's desired folderr
COPY . /usr/share/nginx/html/

# Expose the desired port
EXPOSE 80

# Start the Nginx server
CMD ["nginx", "-g", "daemon off;"]
